<?php
define('SITE_ENGINE_VERSION','v0.0.1');


//  === dev ====  //
error_reporting(NULL);
    ini_set('display_errors',1);
    ini_set('display_startup_errors',1);
    error_reporting(E_ALL);

//  ==== session ==== //
session_name('legral-conferenciers');
session_start();
//session_regenerate_id();

if (isset($_GET['deconnect'])){
    $_SESSION = array();
    session_destroy();
}


//  === bufferisation ====  //
ob_start();


//  ==== configuration du projet ==== //
##require('../release.php');
require("./configLocal/config.php");


//  ==== constantes generales ==== //
//define('DATENOW',date('Y/m/d'));
$protocole=(isset($_SERVER["HTTPS"]))?'https://':'http://';
define('ARIANE_URI',$_SERVER["REQUEST_URI"]);
define('ARIANE_FULL',$protocole.$_SERVER["SERVER_NAME"].ARIANE_URI);
unset($protocole);


// = ======================= = //
// = gestion du mot de passe = //
// = ======================= = //
/* $P_ETAT:etat du password
 * -1: non demandé
 * 0 demander non fournis
 * 1: fournis mais incorrect
 * 2: fournis ok
 */
$P_ETAT=-1; // par defaut pas de mot de passe demandé
/*
// $isPassAsk: etat de la demande du password, demande faite par le routeur (ensemble ou une page)
// 0: pas de demande de pawwd
// 1: demande faite
 $isPassAsk=0;   // 1: si un routeur a demander un mot de passe (permet d'activer un passwd general

//  === recuperation du mdp ====  //
$P_GIVE=NULL;
if(isset($_POST['p'])){$P_GIVE=$_POST['p'];}
elseif (isset($_SESSION['p'])){$P_GIVE=$_SESSION['p'];}
define ('P_GIVE',$P_GIVE);
$_SESSION['p']=$P_GIVE;
unset($P_GIVE);

//  === mot de passe globale requis ====  //
//if (P_GIVE !== PWD ){$isPassAsk=1;}
 */

//  === Librairies ====  //
require('vendors/legral/gestLib/gestLib.php');
$gestOut->setIsFile();
$gestOut->setIsReturn(0);
$gestOut->writeHeader('<meta charset="utf-8" /><link rel="stylesheet" href="http://legral.fr/lib/legral/php/gestLib/gestLib.css">');
$gestOut->writeDate();
require('modeles/_modeleMVC/dev.php');
define('MINIMIZE_SUFFIXE',DEV_LVL>0?'':'.min');

require('modeles/_modeleMVC/templates.php');

// - gestLogins - //
require('vendors/legral/gestLogins/sid/gestLogins.php');
$legralUsers=new GestLogins(PROJET_NOM.'_');
require('configLocal/logins.php');
require('modeles/_modeleMVC/logins.php');

// - cachePSP - //
require('vendors/legral/cachePSP/sid/cachePSP.php');
$cachePSP=new CachePSP();
$cachePSP->setCacheRoot(CACHE_ROOT);
$cachePSP->setUtilisateur(LOGIN);
$cachePSP->setEngine(0);
// = ======== = //
// = dataBase = //
// = ======== = //
// geré par le controleur


// = ========= = //
// = modeleMVC = //
// = ========= = //

// - route - //
$route=ROUTE_DEFAUT;
$routeurFile=ROUTE_DEFAUT.'.php';

$controleurFile=CONTROLEURFILE_DEFAUT;//controleur par default
//$controleurFile='page/accueil.php'; 


$pageBase=PAGE_ROOT;
$isPageExist=1;
$pageDefault=PAGE_DEFAUT;
$pageAsk=PAGE_DEFAUT;  // page demander (code GET)
$pagePath=PAGE_DEFAUT; // chemin (relatif ou absolu) + nom +(NOT ext) du fichier a charger
$pageExt=PAGE_EXT_DEFAUT;

$titleHTML=TITLE_HTML_DEFAUT; //titleHTML


// = ======= = //
// = routeur = //
// = ======= = //
// - surcharge par GET: selection du routeur par priorité - //
if (isset($_GET[PROJET_NOM]))   {$route=PROJET_NOM;     $pageAsk=$_GET[PROJET_NOM];}
elseif (isset($_GET['_modeleMVC'])) {$route='_modeleMVC';     $pageAsk=$_GET['_modeleMVC'];}


// - constantes (pré-route) - //
define ('ROUTE',$route);
define ('ROUTEUR_FILE',ROUTE.'.php');
define ('PAGE',$pageAsk);   // avant le routeur
unset($route,$pageAsk);

$pageBase=PAGE_ROOT.ROUTE.'/';
require('routeurs/'.ROUTE.'/'.ROUTEUR_FILE);


// = ============================== = //
// = gestion des pages inexistantes = //
// = ============================== = //
define ('PAGE_EXIST',$isPageExist);unset($isPageExist);
define ('FICHIER_PATH',$pageBase.$pagePath.$pageExt);
define ('FICHIER_EXIST',file_exists(FICHIER_PATH));

if (PAGE_EXIST === 0       // -- detecté par le routeur -- //
 OR FICHIER_EXIST === FALSE// -- fichier physiquement inexistant -- //
){
    // surcharger la page '_pageNoExist' defini par le routeur
    $pageBase=PAGE_ROOT;
    $pagePath='_modeleMVC/_pageNoExist';
    $pageExt='.php';
}

define ('PAGE_BASE',$pageBase);

// - constantes (post-route) - //
define ('CONTROLEUR_FILE',$controleurFile);
define ('PAGE_PATH',$pagePath);
define ('PAGE_EXT',$pageExt);

define('TITLEHTML',($titleHTML===NULL?TITLE_HTML_DEFAUT:$titleHTML));
unset($titleHTML,$route,$routeurFile,$controleurFile,$pageBase,$pageDefault,$pageAsk,$pagePath,$pageExt,$ariane,$pspV);

// - si pas de password demandé: sauvegarde de la route et de la page  - //
if ($P_ETAT === -1){
    $_SESSION['redir_route']=ROUTE;
    $_SESSION['redir_page']=PAGE;
}


// = ========== = //
// = controleur = //
// = ========== = //
require('controleurs/'.PROJET_NOM.'/'.PROJET_NOM.'.php');
if (CONTROLEUR_FILE !== NULL and file_exists('controleurs/'.CONTROLEUR_FILE))require('controleurs/'.CONTROLEUR_FILE);

// = ==== = //
// = vues = //
// = ==== = //
try{
    
    if ($cachePSP->showCache()===1){
        gestLib_inspect('Appel du cache: ',$cachePSP->getCacheFile());

        // - ajout du code JS, en fin de page indiquant la duree de construction de la page (incluant le cache) - //
        define('PAGE_DUREE_CONSTRUCTION',microtime(TRUE)-SCRIPTDEBUT);
        $js='<script>//ajouter par index.php'."\n";
        $js.='poId=document.getElementById("cacheInfos")'."\n";
        $js.='var cachePSP_pageDureeContruction='.PAGE_DUREE_CONSTRUCTION.";\n"; // code integrer au cache
        $js.='var gain=cachePSP_cacheDureeContruction-cachePSP_pageDureeContruction;'."\n";
        $js.='var ratio=100*cachePSP_cacheDureeContruction/cachePSP_pageDureeContruction;'."\n";

        $js.='if ( poId != undefined){'."\n";
        $js.='poId.innerHTML+="Page générée en '.number_format(PAGE_DUREE_CONSTRUCTION,6).'s.";'."\n";
        $js.='poId.innerHTML+=" Cache contruit en:"+cachePSP_cacheDureeContruction.toFixed(6)+"s.";'."\n";
        $js.='poId.innerHTML+=" Gain: "+gain.toFixed(6)+"s";'."\n";
        $js.='poId.innerHTML+=" Ratio: "+ratio.toFixed(3)+"%";'."\n";
        $js.='}</script>'."\n";
        echo $js;
    }
    else{// pas de cache: on construit
        require (PAGE_ROOT.PROJET_NOM.'/index.php');
    }


    // - sauvegarde du cache et affochage du buffer - //
    $cachePSP->saveCache();
    ob_end_flush();

}catch (Exception $e){

    echo gestLib_inspect('$e',$e);
    echo 'Exception reçue : ',  $e->getMessage(), "\n";
    $_SESSION = array();
    session_destroy();

}

