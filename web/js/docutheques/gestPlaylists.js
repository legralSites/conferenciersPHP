/*
 * gestPlaylists.js
 * gestion d'une playlist simple independant du media
 * */
function gestPlaylist(){
    this.serveurPath="";
    this.root="";
    this.qualityPath="";
    this.path=this.serveurPath+this.root+this.qualityPath;
    
    this.loop=0;
    this.index=0

    this.datas=[];
    //track
}


gestPlaylist.prototype={

    getNb:function(){
        return this.datas.length;
    }
    ,addItem: function (item){
        this.datas.push(item);
    }

    // renvoie le prochain index de la playlist (renvoi le 1er si dernier)
    ,next:function(){
        if(this.index < this.datas.length-1) return ++this.index;
        if (this.loop===1)return this.index=0;
    }

    ,prev:function(){
        if(this.index > 0) return --this.index;
        if (this.loop===1)return this.index=this.datas.length-1;
    }


    //
    ,setLoop:function(loop=1){
        this.loop=loop===1?1:0;
    }
    ,additem(item={}){
        newIndex=this.datas.length-1;
        this.datas[newIndex]=item
    }

} // gestPlaylist.prototype



/*
 * gere les controls de la playlist
 */
function playlistControl(htmlId,init){

    if (typeof(init)!=="object")return -1;
    if (this.htmlId=document.getElementById(htmlId)===undefined) return undefined
    this.name=init.name===undefined?"":init.name;  // nom du control (info)

 //   this.texte=         init.texte===undefined?"":init.texte; // texte afficher

    this.cmdOnIconeUrl=init.cmdOnIconeUrl===undefined?"":init.cmdOnIconeUrl;
    this.cmdOnTexte=   init.cmdOnTexte   ===undefined?"":init.cmdOnTexte; // sert de alt a l'icone
    this.cmdOnClass=   init.cmdOnClass   ===undefined?"":init.cmdOnClass; 
    this.cmdOnCallback=function(){};

    this.cmdOffIconeUrl=init.cmdOffIconeUrl===undefined?"":init.cmdOffIconeUrl;
    this.cmdOffTexte=   init.cmdOffTexte   ===undefined?"":init.cmdOffTexte;
    this.cmdOffClass=   init.cmdOffClass   ===undefined?"":init.cmdOffClass; 
    this.cmdOffCallback=function(){};

}

playlistControl.prototype={
    create:function(){
    }
}

