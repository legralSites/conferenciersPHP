var openNb=0;
var msgNb=0;
//var source = new EventSource('http://127.0.0.1/git/sites/_modeleMVC-extras/sid/vues/js/demo_sse.php');

//var source = new EventSource('http://127.0.0.1/git/sites/docutheques/sid/modeles/docutheques/flux.php');

var source = new EventSource('http://127.0.0.1/git/sites/docutheques/sid/modeles/docutheques/flux.php?salon=legral');
gestConsoles.write('debug',gestLib_inspect('source',source));
gestConsoles.write('debug','<hr>');

var dataOld='';
var isDataNew=0;
var tsOld=0;
var ts=0;

var srcOld='';

source.onopen=function(){
    gestConsoles.clear('debug');
    gestConsoles.clear('eventInfos');
    gestConsoles.clear('flux');
    openNb++;
    gestConsoles.write('debug','<u>onopen:</u>'+openNb);
}

source.onerror=function(e) {
    //gestConsoles.clear('debug');
    //gestConsoles.write('debug',"EventSource failed.");
    gestConsoles.write('erreur',"EventSource failed:"+gestLib_inspect('e',e));
}

source.onmessage = function(event) {
    msgNb++;

    var ts=event.timeStamp;
    var duree=ts-tsOld;
    tsOld=ts;
    gestConsoles.write('debug','<hr>');
    gestConsoles.write('debug','duree='+addCommas(duree/1000000)+'s');
    //gestConsoles.write('eventInfos',gestLib_inspect('event',event));


    var dataJSON=JSON.parse(event.data);
    gestConsoles.write('eventInfos',gestLib_inspect('dataJSON',dataJSON));



    if ( dataJSON.url !== dataOld){
        dataOld=dataJSON.url;
        isDataNew=1;
    }
    else{
        isDataNew=0;
    }

    if (isDataNew===1){

        gestConsoles.write('flux',event.data);

        // - mise a jours du player - //
        var playerId=document.getElementById('player');
        var playerTitre=document.getElementById('playerTitre');

        playerId.src=dataJSON.url;
        playerId.autoplay=true;

        gestConsoles.write('eventInfos',gestLib_inspect('playerId.src',playerId.src));

        playerTitre.innerHTML=dataJSON.titre;
        //gestConsoles.write('eventInfos',gestLib_inspect('playerTitre.innerHTML',playerTitre.innerHTML));
        //gestConsoles.write('eventInfos',gestLib_inspect('dataJSON.url',dataJSON.url));
    }
    else{
        gestConsoles.write('eventInfos',"Données inchangés");
    }
};

//source.addEventListener("ping", function(event) {
//    gestConsoles.write('debug','addEventListener("ping")<u>:</u>');
//    gestConsoles.write('flux',event.data);
//}, false);

