// #################### # //
// # gestion des videos # //
// #################### # //

// - fonctions generiques et independantes - //
function playPause(htmlId) { 
    if (myVideo = document.getElementById(htmlId) === undefined) return -1 ; 

    if (myVideo.paused) 
            myVideo.play(htmlId); 
        else 
            myVideo.pause(htmlId); 
} 


function makeSmall(htmlId) { 
var myVideo = document.getElementById(htmlId); 
    myVideo.width = 320; 
} 

function makeNormal(htmlId) { 
var myVideo = document.getElementById(htmlId); 
    myVideo.width = 420; 
} 

function makeBig(htmlId) { 
var myVideo = document.getElementById(htmlId); 
    myVideo.width = 560; 
} 


// - - // 
// use gestlists.js

playlistIndex=0;


function playlistIndexShow(){
    playlistIndexId=document.getElementById('playlistIndex');
    playlistIndexId.innerHTML=playlistIndex;
}

// - change la source de la video en fonction du select - //
function playlistSelect(vId,videoSelectId,callBack){
    playlistIndex=videoSelectId.options.selectedIndex;
    vId.src=videoSelectId.options[playlistIndex].value;
    playlistIndexShow();
    //vId.load();
    //vId.play();
}


// - renvoie la playlist sous forme d'option - //
function playlistOption(playlist,callBack=function(){}){
    var o='';
    for (i=0;i<playlist.length;i++){
        o+='<option value="'+playlist[i].url+'">'+i+':'+playlist[i].nom+'</option>';
    }
    return o;
}



// - lit l'element de la playlist dans la balise video - //
function playlistPlay(vId,playlist,videoSelectIdHTML,callBack=function(){}){
    if (playlist[playlistIndex].url !== undefined){
        vId.play();
        playlistIndexShow();
        return playlistIndex;
    }
    return -1;
}

// - Met en pause la balise video  - //
function playlistPause(vId,playlist,videoSelectIdHTML,callBack=function(){}){
    if (playlist[playlistIndex].url !== undefined){
        vId.pause();
        playlistIndexShow();
    return playlistIndex;
    }
    return -1;
}



// - dec l'element de la playlist  - //
// si elt=0 atteint -> ne rien faire
function playlistPrev(vId,playlist,videoSelectIdHTML,callBack=function(){}){
    if (playlistIndex > 0 && (playlist[playlistIndex-1].url !== undefined)){
        playlistIndex--;

        // maj du select
        videoSelectId=document.getElementById(videoSelectIdHTML);
        videoSelectId.options.selectedIndex=playlistIndex;
        videoSelectId.selectedIndex=playlistIndex;
        
        vId.src=videoSelectId.options[playlistIndex].value;
        vId.play();
        playlistIndexShow();
    }
    return playlistIndex;
}

// - inc l'element de la playlist  - //
// si eltMax atteint -> ne rien faire
function playlistNext(vId,playlist,videoSelectIdHTML,callBack=function(){}){
    if (playlistIndex <  playlist.length - 1){
        playlistIndex++;

        // maj du select
        videoSelectId=document.getElementById(videoSelectIdHTML);
        videoSelectId.options.selectedIndex=playlistIndex;
        videoSelectId.selectedIndex=playlistIndex;

        vId.src=videoSelectId.options[playlistIndex].value;
        vId.play();
        playlistIndexShow();
    }
    return playlistIndex;
}

