<?php
gestOut_file(__FILE__);

$pspN='conferenceRef';
$pspV=NULL;
if (isset($_GET[$pspN])){$pspV=$_GET[$pspN];}
elseif (isset($_POST[$pspN])){$pspV=$_POST[$pspN];}
elseif (isset($_SESSION[$pspN])){$pspV=$_SESSION[$pspN];}
$pspV=str_replace('/','',$pspV);
define('CONFERENCE_REF',$pspV);
$_SESSION[$pspN]=CONFERENCE_REF;



// = =========== = //
// = mediaLegral = //
// = =========== = //
define('FLUX_RADIOLEGRAL','http://docutheques.legral.fr:39100');
//define('FLUX_LEGRALTV',file_get_contents(VAR_ROOT.'flux/legralTV.txt'));
//define('FLUX_METATV',file_get_contents(VAR_ROOT.'flux/metaTV.txt'));


// = ========== = //
// = controleur = //
// = ========== = //

//  ==== lien https ==== //
$host  = $_SERVER['HTTP_HOST'];
$uri   = $_SERVER['REQUEST_URI'];
define ('HTTPS_LINK','https://'.$host.'/'.$uri);
unset($host,$uri);


//  ==== fonctions servant au menu ==== //

function searchGenreToDir($dir,&$tableau){
//    gestOut_file(__FUNCTION__);

    if ($handle = opendir($dir)) {
        //echo "Gestionnaire du dossier : $handle\n";
        //echo "Entrées :\n";

        while (FALSE !== ($entry = readdir($handle))) {
            if ($entry === "." OR $entry === ".." OR (is_dir($dir.DIRECTORY_SEPARATOR.$entry)===FALSE)) {
                continue;
            }
            $tableau[]=$entry;
        }

        closedir($handle);
        natsort($tableau);
    }
    gestLib_inspect('$tableau',$tableau);
}

//  ==== Autres fonctions ==== //

function menu_Docs_Select($fNom,$route,$routeVal,$mediaGenre,$theme,$optionTitre,$rep,$recursif=0){
    /*gestLib_inspect('$fNom',$fNom,__FILE__.':'.__LINE__);
    gestLib_inspect('$mediaGenre',$mediaGenre);
    gestLib_inspect('$theme',$theme);
    gestLib_inspect('$optionTitre',$optionTitre);
    gestLib_inspect('$rep',$rep);
    gestLib_inspect('$recursif',$recursif);

    gestLib_inspect('SUJET',SUJET);
     */

    return '<span class="menu_Docs">'
        .'<form id="'.$fNom.'" method="GET" action="?">'
        .'<input name="'.$route.'" type="hidden" value="'.$routeVal.'">'
        .'<input name="mediaGenre" type="hidden" value="'.$mediaGenre.'">'
        .'<input name="theme" type="hidden" value="'.$theme.'">'
        .'<select name="sujet" onchange="'.$fNom.'.submit()">'
        .'<option>'.$optionTitre.'</option>'
        .dirToArray_Select($rep,SUJET,$recursif=0)
        .'</select></form></span>';
}


// - lit un repertoire et renvoie uniquement les repertoires - //
// retourne sous forme de liste d'option
// $dirSelected: repertorie a mettre en class selected
function dirToArray_Select($dir,$dirSelected,$recursif=0){

    if (file_exists($dir) === FALSE) {return -1;}

    $result = array();
    $cdir = scandir($dir);
    $out='';
    foreach ($cdir as $key => $value){
        if ($value === '.' OR $value ==='..') continue;

        if (is_dir($dir . DIRECTORY_SEPARATOR . $value)){
            if ($recursif===0){
                $selected=$value===$dirSelected?' selected class="selected"':'';
                $out.='<option'.$selected.'>'.$value.'</option>';
            }
            else{
                $out.='<option>'.$value.'</option>';
                $sousDir=dirToArray($dir . DIRECTORY_SEPARATOR . $value);

                // applatir le tableau
                $sousDirNb=count($sousDir);

                for ($s=0;$s<$sousDirNb;$s++){
                    $out.='<option>'.$sousDir[$s].'</option>';
                    //$result[]=$sousDir[$s];
                }
                // echo gestLib_inspect("dirToArray($dir". DIRECTORY_SEPARATOR ."$value)",$sousDir);

            }

        }
    }
   return $out; 
}

// - lit un repertoire et renvoie uniquement les fichiers - //
// retourne sous forme de liste d'option
// $fileSelected: fichier a mettre en class selected
function fileToArray_Select($dir,$filedirSelected,$recursif=0){

    if (file_exists($dir) === FALSE) {return -1;}

    $result = array();
    $cdir = scandir($dir);
    $out='';
    foreach ($cdir as $key => $value){
        if ($value === '.' OR $value ==='..') continue;

        $chemin=$dir . DIRECTORY_SEPARATOR . $value;
        //echo '<option>$chemin:'.$chemin.'</option>';
        if (is_dir($chemin)){
            if ($recursif===1){
                $out.='<option>'.$value.'</option>';
                $sousDir=fileToArray($dir . DIRECTORY_SEPARATOR . $value);

                // applatir le tableau
                $sousDirNb=count($sousDir);

                for ($s=0;$s<$sousDirNb;$s++){
                    $out.='<option>'.$sousDir[$s].'</option>';
                    //$result[]=$sousDir[$s];
                }
                // echo gestLib_inspect("dirToArray($dir". DIRECTORY_SEPARATOR ."$value)",$sousDir);
            }
        }

        if (is_file($chemin)===TRUE){
            $finfos=pathinfo($chemin);
            if ( $finfos['extension']==='json'){
                $out.= '<option>'.$finfos['filename'].'</option>';
            }
        }

    }
   return $out; 
}




/*
 * fichier: livreAudios-livresByAuteur
 * DeTableauIsé: methode sans tableau (2016.11)
 *  
*/

// - lit un repertoire recursivement et renvoie les repertoires et les fichiers - //
/*
 * $wwwDirRoot constante: https://media-serveur/
 $level  : Niveau de recursivité
 */
function docu_scanDir($fileDirRoot,$wwwDirRoot,$dirRel,$classeDir='',$classeFile='',$level=0){

    $fileDirRoot.=DIRECTORY_SEPARATOR;
    $result = '';

    $dirFullNow=$fileDirRoot.DIRECTORY_SEPARATOR.$dirRel.DIRECTORY_SEPARATOR;

    try{
        $cdir = scandir($dirFullNow,SCANDIR_SORT_ASCENDING);
    }
    catch(Exception $e){
        echo 'Exception-------';
        $result.=gestLib_inspect('Exception',$e);
        //$result.='Exception reçue : ',  $e->getMessage(), "<br>\n";
    }


    foreach ($cdir as $key => $value){

        if ($value === '.' OR $value ==='..') continue;

        // Au niveau 0: reinit du repertoire relatif
        if ($level === 0)
            $dirRel='';

        // pour le div head
        $htmlId_head='id_'.$value;
        $htmlId_head_code =' id="'.$htmlId_head.'"';

        // pour le div data
        $htmlId_datas      ='id_'.$value.'_datas';
        $htmlId_datas_code =' id="'.$htmlId_datas.'"';

        $dirRelNow=$dirRel.DIRECTORY_SEPARATOR.$value;
        $dirFullNow=$fileDirRoot.$dirRelNow;


        if(DEV_LVL >= 5){
            $result.=gestLib_inspect('$fileDirRoot',$fileDirRoot);
            $result.=gestLib_inspect('$wwwDirRoot',$wwwDirRoot);
            $result.=gestLib_inspect('$dirRel',$dirRel);
            $result.=gestLib_inspect('$dirFullNow',$dirFullNow);
            $result.=gestLib_inspect('$dirRelNow',$dirRelNow);
            $result.=gestLib_inspect('$level',$level);
        }   

        if (is_dir($dirFullNow)){

            // - head - //
            $result.='<div class="'.$classeDir.'_head" onclick="blockSwitch(\''.$htmlId_datas.'\')">'.$value.'</div>'."\n";

            // - datas - //
            $result.='<div'.$htmlId_datas_code.' style="display:none" class="'.$classeDir.'_datas">'."\n";
 
            // -- scan du contenu du rep -- //
            $result.=docu_scanDir($fileDirRoot,$wwwDirRoot,$dirRelNow,$classeDir,$classeFile,$level+1);
        }
        else{
            // - fichier - //
            $result.='<div'.$htmlId_datas_code.' style="display:block" class="'.$classeFile.'">'."\n";
            $result.=docu_analyseFile($fileDirRoot,$wwwDirRoot,$dirRelNow,$value);
        }

        $result.='</div>'."\n";// datas ou fichier
    }
    return $result."\n";

}

/*
 * $wwwDir= $wwwDirRoot . $wwwDirRel
 * $dirRel: rep courant + fichierNom
 * $fichierNom: pour info
 * */

$iterNb=0;

function docu_analyseFile($fileDirRoot,$wwwDirRoot,$dirRel,$fichierNom){
global $iterNb;
    $o='';
//    $dirRel.=DIRECTORY_SEPARATOR;

    $filePath=$fileDirRoot.$dirRel;
    $path=$wwwDirRoot.$dirRel;
    $finfo=pathinfo($path);

    $embedId=uniqid();
    if(DEV_LVL >= 5){
        $o.=gestLib_inspect('$fileDirRoot',$fileDirRoot);
        $o.=gestLib_inspect('$wwwDirRoot',$wwwDirRoot);
        $o.=gestLib_inspect('$dirRel',$dirRel);
        $o.=gestLib_inspect('$filePath',$filePath);
        $o.=gestLib_inspect('$fichierNom',$fichierNom);
        $o.=gestLib_inspect('$path',$path);
    }
    if (isset($finfo['extension']))    switch($finfo['extension']){
        case 'ogg':case 'mp3':case 'flac':case 'm4a':

            $o.='<div class="">'.$finfo['filename'];
            $o.='<img alt="'.$finfo['extension'].'" src="web/img/docutheques/logo-'.$finfo['extension'].'.png" style="height:1rem;margin-left:1rem;">';
            $o.='</div>';

            // - id3 - //
            require_once('getid3/getid3.php');
            $getID3=new getID3;
            //echo gestLib_inspect('$getID3',$getID3);
            $id3_infos=$getID3->analyze($filePath);
            //getid3_lib::CopyTagsToComments($id3_infos);
            //$id3_tags=&$id3_infos['tags'];
            //$id3_tagsID3v2=&$id3_infos['id3v2'];
            //$id3_tagsID3v2_champs=&$id3_infos['id3v2']['comments'];
            $id3_tagsID3v2_champs=&$id3_infos['tags_html']['id3v2'];
            $id3_year  =isset($id3_tagsID3v2_champs['year'][0])  ?$id3_tagsID3v2_champs['year'][0]:'';
            $id3_artist=isset($id3_tagsID3v2_champs['artist'][0])?$id3_tagsID3v2_champs['artist'][0]:'';
            $id3_album =isset($id3_tagsID3v2_champs['album'][0]) ?$id3_tagsID3v2_champs['album'][0]:'';
            $id3_genre =isset($id3_tagsID3v2_champs['genre'][0]) ?$id3_tagsID3v2_champs['genre'][0]:'';
            $id3_comment=isset($id3_tagsID3v2_champs['comment'][0]) ?$id3_tagsID3v2_champs['comment'][0]:'';
            $id3_playtime_string=isset($id3_infos['playtime_string'])?$id3_infos['playtime_string']:'';

            $id3_errors=isset($id3_infos['error'])?$id3_infos['error']:NULL;
            if(DEV_LVL >= 2){
                //$o.=gestLib_inspectOrigine('$id3_infos',$id3_infos);
                $o.=gestLib_inspect('$id3_errors',$id3_errors);

            }

            if($iterNb === 0){
                $iterNb = 1;
                //$o.=gestLib_inspectOrigine('$id3_infos',$id3_infos);
                //$o.=gestLib_inspectOrigine('$id3_infos[tags]',$id3_infos['tags']);
                //$o.=gestLib_inspectOrigine('$id3_tags',$id3_tags);
                //$o.=gestLib_inspectOrigine('$id3_tagsID3v2',$id3_tagsID3v2);
                //$o.=gestLib_inspectOrigine('$id3_tagsID3v2_champs',$id3_tagsID3v2_champs);
                
                //$o.=gestLib_inspect('$id3_infos["tags"]["id3v2"]',$id3_infos['tags']['id3v2']);
                //if(!empty($id3_infos['comments_html']['artist']))$o.='artist: '.$id3_infos['comments_html']['artist'];
                //$track_duree=$id3_infos['playtime'];
                //$playtime_tring=$id3_infos['playtime_string'];
                //$o.='duree: '.$playtime_tring;
            }

            // - affichage id3 - //
            $o.='<div class="id3">';
            if($id3_artist!=='')$o.='<span class="id3_tagName">Artist</span><span class="id3_tagValue">'.$id3_artist.'</span>';
            if($id3_year!=='')$o.='<span class="id3_tagName">Année</span><span class="id3_tagValue">'.$id3_year.'</span>';
            if($id3_album!=='')$o.='<span class="id3_tagName">Album</span><span class="id3_tagValue">'.$id3_album.'</span>';
            $o.='<span class="id3_tagName">Durée</span><span class="id3_tagValue">'.$id3_playtime_string.'</span>';
            $o.='</div>';

            // - affichage balise - //
            $o.='<audio id="'.$embedId.'" controls preload="none"><source src="'.$path.'"></audio><br>';
            break;


        case 'png':case 'jpg':case 'jpeg':
            //$o.='<div class="txtcenter">'.$finfo['filename'].'</div>';
            $o.='<img  id="'.$embedId.'" alt="" src="'.$path.'" /><br>';
            break;


        case 'webm':case 'mp4': case 'avi':
            $o.='<div class="">'.$finfo['filename'];
            $o.='<img alt="'.$finfo['extension'].'" src="web/img/docutheques/logo-'.$finfo['extension'].'.png" style="height:1rem;margin-left:1rem;">';
            $o.='</div>';
            $o.='<video id="'.$embedId.'" controls preload="none"><source src="'.$path.'"></video>';
            break;


        case 'hr':
            $o.='<hr>';

        case 'h1':
            $o.='<h1>'.$finfo['filename'].'</h1>';
            break;
        case 'h2':
            $o.='<h2>'.$finfo['filename'].'</h2>';
            break;
        case 'h3':
            $o.='<h3>'.$finfo['filename'].'</h3>';
            break;
        default:
            $o.'Pas de fichier compatible.<br>';
    }
   // $id=$htmlId===NULL?'': ' id="'.$htmlId.'"';
    //return '<div'.$id.' class="'.$classeFile.'">'.$o.'</div>'."\n";
    return $o."\n";
}


