<?php echo gestOut_file(__FILE__);?>

<form id="fconferencesPubliques" method="POST" action="?conferenciers=show">
    <select name="conferenceRef" onchange="fconferencesPubliques.submit()">
        <option value="0">conference publique:</option>
        <option>conference non existant</option>
        <?php echo fileToArray_Select(DATA_PUBLIQUE_ROOT,CONFERENCE_REF,$recursif=0) ?>
    </select>
</form>

<?php
if (is_file(DATA_PUBLIQUE_ROOT.CONFERENCE_REF.'.json')===FALSE){
    echo 'La conference publique avec la reference "'.CONFERENCE_REF.'" est introuvable!';
    return;
}
?>

<h2>requete Ajax pour charger la playlist de la conference</h2>
<script>
function gestLib_inspect(varNom,varPtr){
    out='';

    varType=typeof(varPtr);
    switch (varType){
        case 'object':
            out+='<span class="gestLibVarNom">'+varNom+'</span>=['+varType+'] <span class="gestLibVar">'+varPtr+'</span><br>';

            for ( i in varPtr){
                out+='&nbsp;&nbsp;'+gestLib_inspect(i,varPtr[i]);
            }
            break;
    default:
            out+='<span class="gestLibVarNom">'+varNom+'</span>=['+varType+'] <span class="gestLibVar">'+varPtr+'</span>';

    }
    return out+"<br>";
}
</script>

<script>
function addCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? ',' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}
</script>

<script>
gestConsoles.consoleAdd({nom:'flux'});
gestConsoles.consoleAdd({nom:'eventInfos'});
gestConsoles.consoleAdd({nom:'debug',genre:'div',classe:''});

gestConsoles.createDiv('flux');
gestConsoles.createDiv('eventInfos');
gestConsoles.createDiv('debug');


//gestConsoles.write('flux','flux:');
gestConsoles.write('eventInfos','eventInfos:');

gestConsoles.write('debug','Test de la console de debug.');

</script>

<script>

var openNb=0;
var msgNb=0;
//var source = new EventSource('controleurs/conferenciers/getRun.php');
//var source = new EventSource('https://legral.fr/_modeleMVC/vues/js/demo_sse.php');
var source = new EventSource('run/sse.php');
gestConsoles.write('debug',gestLib_inspect('source',source));

var dataOld='';
var isDataNew=0;
var ts=tsOld=0;

source.onopen=function(){
    gestConsoles.clear('debug');
    gestConsoles.clear('eventInfos');
    gestConsoles.clear('flux');
    openNb++;
    //gestConsoles.write('flux','ouverture');
    //gestConsoles.clear('debug');
    gestConsoles.write('debug','<u>onopen:</u>'+openNb);
}

source.onerror=function(e) {
    gestConsoles.write('debug',"EventSource failed.");
    //gestConsoles.clear();
    gestConsoles.write('eventInfos',"EventSource failed:"+gestLib_inspect('e',e));
}

source.onmessage = function(event) {
    msgNb++;

    gestConsoles.write('eventInfos','<u>onmessage:</u>'+msgNb+':'+event.id);
    //gestConsoles.write('eventInfos',gestLib_inspect('source',source));

    ts=event.timeStamp;
    duree=ts-tsOld;
    tsOld=ts;
    gestConsoles.write('debug','duree='+addCommas(duree/1000000)+'s');
    //gestConsoles.write('eventInfos',gestLib_inspect('event',event));

    if (event.data != dataOld){
        dataOld=event.data;
        isDataNew=1;
    }
    else{
        isDataNew=0;
    }

    //    if (msgNb<10){
    //        gestConsoles.write('debug', event.data);
    //    }

    // - traitement des datas - //

    //gestConsoles.clear('flux'); // deplacer dans open
    //gestConsoles.write('flux',event.data);
    gestConsoles.write('flux',event.data);
    //var dataJSON=JSON.parse(event.data);
    //gestConsoles.write('flux',dataJSON);
    //gestConsoles.write('flux',gestLib_inspect('dataJSON',dataJSON));
};
</script>

<script>

source.addEventListener("ping", function(event) {
    gestConsoles.write('debug','addEventListener("ping")<u>:</u>');
    gestConsoles.write('flux',event.data);
}, false);
</script>


<h2></h2>

<h2></h2>


<?php
/*
//Ce code fonctionne
echo '<p>Lit le fichier</p>';

$fileContent=file_get_contents(DATA_PUBLIQUE_ROOT.CONFERENCE_REF.'.json');

echo '<p>ecrit les données du fichier</p>';
echo $fileContent;

echo '<p>parse le json</p>';
$confDatas=json_decode($fileContent,TRUE);

echo '<p>Manipule les données PHP</p>';
$gestOut->setIsReturn(-1);

echo gestLib_inspect('$confDatas',$confDatas);
$slides=$confDatas['slides'];
echo gestLib_inspect('$confDatas["slides"]',$slides);
*/





