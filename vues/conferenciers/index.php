<!DOCTYPE html><html lang="fr">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="author" content="Pascal TOLEDO">
    <meta name="generator" content="vim">
    <meta name="identifier-url" content="http://legral.fr">
    <meta name="date-creation-yyyymmdd" content="20170720">
    <meta name="date-update-yyyymmdd" content="20161125">
    <meta name="revisit-after" content="never">
    <meta name="category" content="">
    <meta name="publisher" content="legral.fr">
    <meta name="copyright" content="pascal TOLEDO">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo TITLEHTML;?></title>
    <?php //echo $meta;unset($meta);?>

    <!-- scripts et styles -->
<?php
    echo '<script src="./web/locales/scripts'.MINIMIZE_SUFFIXE.'.js"></script>';
    echo '<link rel="stylesheet" href="./web/locales/styles'.MINIMIZE_SUFFIXE.'.css" media="all" />';
?>
    <script>
    </script>

</head>

<body>
<div id="page">

    <!-- header -->
    <div id="header">
        <div id="headerGauche"></div>
        <h1><a href="?">Conf&eacute;renciers</a> - <?php echo SITE_ENGINE_VERSION?></h1>

        <!-- menus -->
        <nav id="nav">
<?php 
include 'menus/'.PROJET_NOM.'/'.PROJET_NOM.'.php';
//include 'menus/docutheques/dl.php';
?>
        </nav>

        <div id="headerDroit">
            <?php include 'menus/_modeleMVC/menuSelect.php';?>
        </div>
    </div><!-- header -->
<?php
    include PAGE_BASE.PAGE_PATH.PAGE_EXT;

    include 'vues/_modeleMVC/footer.php';
//    include 'vues/footer.php';
?>

</div><!-- //page -->

<script>
//gestAncres();
</script>
<a id="end"></a>
</body></html>
