<?php
gestOut_file(__FILE__);
if ($legralUsers->isDomaine('LivresAudios')===0) {return;}
?>
<div>
    <form id="flivreAudiosByAuteur" method="GET" action="?">
        <input name="docutheques" type="hidden" value="livreAudios-livresByAuteur">
        <select name="auteur" onchange="flivreAudiosByAuteur.submit()">
            <option>Livres Audios</option>
            <?php echo dirToArray_Select(MEDIASERVEUR_ROOT.'litteratures/livreAudios/Auteurs/',AUTEUR,$recursif=0); ?>
        </select>
     </form>
</div>
