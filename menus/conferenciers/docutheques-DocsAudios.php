<?php
echo gestOut_file(__FILE__);
if ($legralUsers->isDomaine('DocsAudios')===0) {return;}
?>
<div class="menu_Docs">
    <div class="menu_Docs_Titre">DocsAudios</div>

<div class="menu_Docs_content">
<?php
$mediaGenre='audios';//doit correspondre au repertoire
foreach ($themes[$mediaGenre] as $theme){
    echo '<div>'
        .menu_Docs_Select(
            $fNom='fDocs'.$mediaGenre.$theme
            ,$route='docutheques'
            ,$routeVal='docsAudiosVideos'
            ,$mediaGenre
            ,$theme
            ,$optionTitre=$theme
            ,$rep=MEDIASERVEUR_ROOT.'docutheques/'.$mediaGenre.'/'.$theme
            ,$recursif=0)
        .'</div>';
}
?>
    <div class="ms"><!-- hpei -->

<?php
    $p='hpei';
    $css=(ROUTE===$r and PAGE===$p)?' class="actif"':'';
    echo '<li '.$css.'><span '.$css.'><a '.$css.' href="?'.$r.'='.$p.'" title="">Histoires Possibles et Impossibles</a></span></li>';
?>
    </div><!-- hpei -->
</div>

</div><!--id="DocsAudios"-->

