<?php
require("../../configLocal/config.php");

function lastLine($fichier){
    //return 'fonction'."\n\n\n\n";
    //$data=array();
    $data=file($fichier, FILE_SKIP_EMPTY_LINES );
        //return $data."\n\n\n\n";
        //return $data[0]."\n\n\n\n";
        $c=count($data)-1;
        //return $c."\n\n\n\n";
        return $data[$c]."\n\n\n\n";
}


$salon=isset($_GET['salon'])?$_GET['salon']:'legral';

$runPath=VAR_ROOT.'salons/'.$salon.'/run/'.$salon.'.txt';
$archivesPath=VAR_ROOT.'salons/'.$salon.'/archives/'.$salon.'.txt';

//echo gestLib_inspect('$runPath',$runPath);
//echo gestLib_inspect('$archivesPath',$archivesPath);

// - test existance du fichier - //
if (file_exists($runPath) === FALSE){
    echo 'data: {"type":"erreur", txt:"le fichier du salon '.$salon.' est introuvable."}'."\n\n";
    flush();
    return;
}


// - Test de la modification du fichier - //
//$fileFlux_lastModif=0;


//while (TRUE) {

//    $fileFlux_NewModif =filectime($fileFluxPath);

//    if ($fileFlux_NewModif !== $fileFlux_lastModif ){

        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

        $fileFlux_lastModif=$fileFlux_NewModif;
    
        // sortie //
        //echo ':Commentaire sert a maintenir la connexion active'."\n\n";
        echo "\n\n";  // ligne vide

        // - info de debug- //
        //echo 'data: {"fileFlux_lastModif":"'.$fileFlux_lastModif.'","fileFlux_NewModif":"'.$fileFlux_NewModif.'"}'."\n\n\n\n";

        // - data - //
        //    echo 'data: {"titre":"meta", "url":"http://'.$salon.'"}'."\n\n\n\n";
        //    echo 'data: '.$salon."\n\n\n\n";
        echo 'data: '.lastLine($runPath);
        //echo 'data: ';readfile($fileFlux);echo "\n\n\n\n";
        //ob_flush();
        flush();

//    }
//    sleep(1);
//}
